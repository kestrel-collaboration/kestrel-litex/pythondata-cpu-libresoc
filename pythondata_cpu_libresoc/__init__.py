import os.path
__dir__ = os.path.split(os.path.abspath(os.path.realpath(__file__)))[0]
data_location = os.path.join(__dir__, "hdl")
src = "https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-litex/pythondata-cpu-libresoc"

# Module version
version_str = "0.0.post1"
version_tuple = (0, 0, 1)
try:
    from packaging.version import Version as V
    pversion = V("0.0.post1")
except ImportError:
    pass

# Data version info
data_version_str = "0.0.post1"
data_version_tuple = (0, 0, 1)
try:
    from packaging.version import Version as V
    pdata_version = V("0.0.post1")
except ImportError:
    pass
data_git_hash = "087b7f0d2302abf9e82a32253b395f1c60237420"
data_git_describe = "v0.0-1-g087b7f0"
data_git_msg = """\
commit 087b7f0d2302abf9e82a32253b395f1c60237420
Author: Luke Kenneth Casson Leighton <lkcl@lkcl.net>
Date:   Wed Jul 6 14:22:54 2022 +0100

    update pinmux submodule, rename to "fabric"
"""

# Tool version info
tool_version_str = "0.0.post1"
tool_version_tuple = (0, 0, 1)
try:
    from packaging.version import Version as V
    ptool_version = V("0.0.post1")
except ImportError:
    pass


def data_file(f):
    """Get absolute path for file inside pythondata_cpu_libresoc."""
    fn = os.path.join(data_location, f)
    fn = os.path.abspath(fn)
    if not os.path.exists(fn):
        raise IOError("File {f} doesn't exist in pythondata_cpu_libresoc".format(f))
    return fn
